The Budkit Framework
====================

Required to run the Budkit platform. Remember to run::

    $ git submodule init
    $ git submodule update

Alternative, download the master branch and extract into the framework/ directory in the root of your Budkit install