<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * date.php
 *
 * Requires PHP version 5.3
 *
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 *
 * @category   Library
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @since      Class available since Release 1.0.0 Jan 28, 2012 2:06:49 PM
 *
 */

namespace Library\Output\Parse\Template;

use Library;
use Library\Output;
use Library\Output\Parse;

/**
 * What is the purpose of this class, in one sentence?
 *
 * How does this class achieve the desired purpose?
 *
 * @category   Libraries
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @since      Class available since Release 1.0.0 Jan 28, 2012 2:06:49 PM
 */
class Date extends Parse\Template {
    static
        $instance ;

    /**
     * Defines the class constructor
     * Used to preload pre-requisites for the element class
     *
     * @return object element
     */
    public function __constructor() {

    }


    final private static function getDaySelect($tag, $value="", $disabled=array()){

        $options    = array();
        $type       = strtolower($tag['TYPE']);
        $disabled   = isset($tag['DISABLED'])? explode(',', $tag['DISABLED']) : $disabled; ;
        $value      = isset($tag['VALUE'])? static::getData($tag['VALUE'], $tag['VALUE']) : $value;
        $name       = isset($tag['NAME']) ? (($type !=="day" )? "{$tag['NAME']}_day" : $tag['NAME']) : "";

        if($type!=="day" && !empty($value)){
            $value = date("j", strtotime($value));
        }
        //The empty option
        $options[] = array("ELEMENT"=>"option", "CDATA"=>"Day");

        for($i=1; $i<32; $i++):

            $option = array("ELEMENT"=>"option","VALUE"=>$i, "SELECTED"=>'', "DISABLED"=>'', "CDATA"=>$i);

            if($type=="day" && in_array($i, $disabled)): $option["DISABLED"]= "disabled";  else: unset($option['DISABLED']); endif;
            if($type=="day" && (int)$value == $i): $option["SELECTED"]= "selected"; else: unset($option['SELECTED']); endif;
            $options[] = $option;
        endfor;

        $select = array("ELEMENT"=>"select", "CLASS"=>"form-control day-control", "CHILDREN"=>$options);

        //Select field name;
        if(!empty($name)){
            $select["NAME"] = $name;
        }
        //Additional attributes
        if($type=="day"){
            unset($tag['TYPE']);
            unset($tag['DISABLED']);
            $select = array_merge($tag, $select); //Just so we get any additional  that are added and we've missed to process
        }
        return $select;

    }

    final private static function getMonthSelect($tag, $value="", $disabled=array()){

        $options    = array();
        $type       = strtolower($tag['TYPE']);
        $disabled   = isset($tag['DISABLED'])? explode(',', $tag['DISABLED']) : $disabled; ;
        $value      = isset($tag['VALUE'])? static::getData($tag['VALUE'], $tag['VALUE']) : $value;
        $name       = isset($tag['NAME']) ? (($type !=="month" )? "{$tag['NAME']}_month" : $tag['NAME']) : "";

        if($type!=="month" && !empty($value)){
            $value = date("n", strtotime($value));
        }
        $options[] = array("ELEMENT"=>"option", "CDATA"=>"Month");
        $months    = array("January","February","March","April","May","June","July","August","September","October","November","December");

        for($i=1; $i<13; $i++):

            $option = array("ELEMENT"=>"option","VALUE"=>$i, "SELECTED"=>'', "DISABLED"=>'', "CDATA"=> $months[($i-1)]);

            if($type=="month" && in_array($i, $disabled)): $option["DISABLED"]= "disabled";  else: unset($option['DISABLED']); endif;
            if($type=="month" && (int)$value == $i): $option["SELECTED"]= "selected"; else: unset($option['SELECTED']); endif;
            $options[] = $option;
        endfor;

        $select = array("ELEMENT"=>"select", "CLASS"=>"form-control month-control", "CHILDREN"=>$options);

        //Select field name;
        if(!empty($name)){
            $select["NAME"] = $name;
        }
        //Additional attributes
        if($type=="month"){
            unset($tag['TYPE']);
            unset($tag['DISABLED']);
            $select = array_merge($tag, $select); //Just so we get any additional  that are added and we've missed to process
        }
        return $select;
    }

    final private static function getYearSelect($tag, $value="", $disabled="", $range="-10", $limit="0"){

        $options    = array();
        $type       = strtolower($tag['TYPE']);
        $disabled   = isset($tag['DISABLED'])? explode(',', $tag['DISABLED']) : $disabled; ;
        $value      = isset($tag['VALUE'])? static::getData($tag['VALUE'], $tag['VALUE']) : $value;
        $range      = isset($tag['RANGE'])? $tag['RANGE'] : $range;
        $limit      = isset($tag['LIMIT'])? $tag['LIMIT'] : $limit;
        $name       = isset($tag['NAME']) ? (($type !=="year" )? "{$tag['NAME']}_year" : $tag['NAME']) : "";

        if($type!=="year" && !empty($value)){
            $value = date("Y", strtotime($value));
        }
        $options[] = array("ELEMENT"=>"option", "CDATA"=>"Year");
        $current = intval( date("Y") );
        $start   = ($current+ (int)$range);
        $end   = ($current+ (int)$limit);

        for($i=$start; $i<($end+1); $i++):

            $option = array("ELEMENT"=>"option","VALUE"=>$i, "SELECTED"=>'', "DISABLED"=>'', "CDATA"=> $i);

            if($type=="year" && in_array($i, $disabled)): $option["DISABLED"]= "disabled";  else: unset($option['DISABLED']); endif;
            if($type=="year" && (int)$value == $i): $option["SELECTED"]= "selected"; else: unset($option['SELECTED']); endif;
            $options[] = $option;
        endfor;

        $select = array("ELEMENT"=>"select", "CLASS"=>"form-control year-control", "CHILDREN"=>$options);

        //Select field name;
        if(!empty($name)){
            $select["NAME"] = $name;
        }
        //Additional attributes
        if($type=="year"){
            unset($tag['TYPE']);
            unset($tag['DISABLED']);
            unset($tag['RANGE']);
            unset($tag['LIMIT']);
            $select = array_merge($tag, $select); //Just so we get any additional  that are added and we've missed to process
        }
        return $select;
    }


    public static function execute($parser, $tag, $writer) {

        //Are we trying to render the date?

        $type = strtolower($tag['TYPE']);
        switch($type):
            case "day":
                $tag = static::getDaySelect($tag);
                break;
            case "month":
                $tag = static::getMonthSelect($tag);
                break;
            case "year":
                $tag = static::getYearSelect($tag);
                break;
            default :
                $tag = array(
                    static::getDaySelect($tag),
                    static::getMonthSelect($tag),
                    static::getYearSelect($tag),
                );
                break;
        endswitch;

        return $tag;
    }

    /**
     * Returns and instantiated Instance of the element class
     *
     * NOTE: As of PHP5.3 it is vital that you include constructors in your class
     * especially if they are defined under a namespace. A method with the same
     * name as the class is no longer considered to be its constructor
     *
     * @staticvar object $instance
     * @property-read object $instance To determine if class was previously instantiated
     * @property-write object $instance
     * @return object element
     */
    public static function getInstance() {

        if (is_object(static::$instance) && is_a(static::$instance, 'select'))
            return static::$instance;

        static::$instance = new self();

        return static::$instance;
    }

}