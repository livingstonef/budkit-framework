<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * media.php
 *
 * Requires PHP version 5.3
 *
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 *
 * @category   Library
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/layout
 * @since      Class available since Release 1.0.0 Feb 5, 2012 10:15:29 PM
 *
 */

namespace Library\Output\Parse\Template;

use Library\Config;
use Library\Output\Parse;
use Library\Uri;
use Platform\Framework;

/**
 * What is the purpose of this class, in one sentence?
 *
 * How does this class achieve the desired purpose?
 *
 * @category   Libraries
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/layout
 * @since      Class available since Release 1.0.0 Feb 5, 2012 10:15:29 PM
 */
class Media extends Parse\Template {
    /*
     * @var object
     */

    static $instance;
    protected static $modes = array("icon", "thumbnail", "detailed");
    protected static $images = array("image/bmp", "image/gif", "image/jpeg", "image/jpeg", "image/jpg");
    protected static $videos = array("video/mp4");
    protected static $audio = array();
    protected static $rich = array(); //Rich Html embed for swf etc

    private static function tag($media, $tag = NULL) {

        $config = \Library\Config::getInstance();

        static::$images = $config->getParam("image", static::$images, "attachments");
        static::$videos = $config->getParam("video", static::$videos, "attachments");
        static::$audio = $config->getParam("audio", static::$audio, "attachments");
        //static::$rich  = $config->getParam( "application", static::$rich, "attachments");

        $tag = empty($tag) ? array() : $tag;
        //Media must be a platform entity object
        //if(!is_a($media, '\Platform\Entity')) return null;
        //$attachmentTypes = \Library\Config::getParam("allowed-types", array(), "attachments");
        $name = $media->getPropertyValue("attachment_name");
        $type = $media->getPropertyValue("attachment_type");
        $uri = $media->getObjectURI();
        $ext = $media->getPropertyValue("attachment_ext"); //get the attachment extension;
        $url = "/system/object/" . $uri;

        $mode = isset($tag['MODE']) ? $tag['MODE'] : "thumbnail"; //thumbnail, icon etc...
        $link = isset($tag['LINK']) ? TRUE : FALSE;

        //if the file does not exists
        if (empty($name) || empty($type) || empty($uri)):
            $uri = 'placeholder';
            $name = 'Placeholder';
            $type = 'image/jpeg';
            $link = false;
        endif;

        $class = isset($tag['CLASS']) ? static::getData($tag['CLASS'], $tag['CLASS']) : null;
        $height = isset($tag['HEIGHT']) ? static::getData($tag['HEIGHT'], $tag['HEIGHT']) : null;
        $width = isset($tag['WIDTH']) ? static::getData($tag['WIDTH'], $tag['WIDTH']) : null;
        $mime = strtolower($type);
        //unset($tag['NAMESPACE']);

        $basename = \Library\Folder\Files::getExtension($name, "file");
        $fileExtension = strtolower($basename);

        //If the medialink is linkable...
        if ($link):
            $tag['ELEMENT'] = 'a';
            $tag['HREF'] = \Library\Uri::internal($url);
        else:
            $tag['ELEMENT'] = 'figure';
            $tag['CLASS'] = "medialink\n" . !empty($class) ? $class : null;
        endif;

        //@TODO determine browser compatibility and add mime to the above arrays i.e images, videos, audio, rich;
        //Can we render this media link?
        $renderable = array_merge(static::$images, static::$videos, static::$audio, static::$rich);

        //Videos, audio rich
        if ((in_array($mime, array_merge(static::$videos, static::$audio, static::$rich)) && in_array($mode, array("detailed", "icon")) ) || !in_array($mime, $renderable)) {

            $width  = \Library\Config::getParam('gallery-thumbnail-width', 200, 'content');
            $height = \Library\Config::getParam('gallery-thumbnail-height', 200, 'content');


            $linkable = array("ELEMENT" => "div", "HREF"=>"", "CLASS" => "media-{$fileExtension} {$mode} media-file {$class}");
            $linkable["CHILDREN"][] = array("ELEMENT" => "span", "CLASS" => "media-type media-{$fileExtension}", "CDATA" => "<i class='icon-file'></i>" . $fileExtension);

            if ($mode === "detailed" || !in_array($mime, $renderable) ):
                $linkable["CHILDREN"][] = array("ELEMENT" => "span", "CLASS" => "media-filename list-hide", "CDATA" => $name);
                $linkable["CHILDREN"][] = array("ELEMENT" => "span", "CLASS" => "media-help list-hide help-block", "CDATA" => $mime);
            endif;

            if ($link && !empty($url)):
                $linkable['ELEMENT'] = 'a';
                $linkable['HREF'] = \Library\Uri::internal($url);
            else:
                unset($linkable['HREF'] );
            endif;
            //We cannot have two a > a
            return $linkable;
        } else {
            //Type specific
            if (!empty($type)):
                //@TODO will need to determine browser support for the various
                //mime types shown here, for instance only safari browsers support image/tiff;
                if (in_array($mime, static::$images)):
                    //Create an image element
                    $imageLink = \Library\Uri::internal("/system/object/" . $uri);
                    $image = array(
                        "ELEMENT" => "img",
                        "SRC" => $imageLink . (!empty($width) ? "/resize/{$width}" . (!empty($height) ? "/{$height}" : null) : null),
                        "ALT" => !empty($name) ? $name : null,
                        "CLASS"=>"img-responsive",
                        "WIDTH" => !empty($width) ? $width : null,
                        "HEIGHT" => !empty($height) ? $height : null
                    );
                    if (empty($image['WIDTH']))
                        unset($image['WIDTH']);
                    if (empty($image['HEIGHT']))
                        unset($image['HEIGHT']);
                    if (isset($link) && !empty($url)):
                        $tag['HREF'] = \Library\Uri::internal("/system/media/photo/view/" . $uri);
                    endif;
                    $tag['CHILDREN'][] = $image;
                    //$tag = array("ELEMENT"=>"span","CDATA"=>"Single Image");
                endif;

                if (in_array($mime, static::$videos) && !empty($uri)):

                    $videoLink = \Library\Uri::internal("/system/object/" . $uri);
                    $tag = static::videoTag($videoLink, $mime, $height, $width, $tag, $ext);

                endif;

                if (in_array($mime, static::$audio) && !empty($uri)):

                    $audioLink = \Library\Uri::internal("/system/object/" . $uri);
                    $tag = static::audioTag($audioLink, $mime, $height, $width, $tag, $ext);

                endif;
            endif;
            unset($tag['WIDTH']);
            unset($tag['HEIGHT']);
        }
        //This is a strange bug, if the children element is the last
        //element in the array, the element is not closed properly.
        unset($tag['NAME']);
        unset($tag['TYPE']);
        unset($tag['URL']);
        unset($tag['URI']);
        unset($tag['MODE']);
        unset($tag['LINK']);
        unset($tag['CDATA']);

        return $tag;
    }

    final private static function figureWrapper($mediaTag,  $width, $height, $type="video"){
        $template       = Config::getParam("template", "default", "design" );
        $templatePath   = Config::getParam("path", "/", "general" ).'public/'.$template;
        return array(
            "ELEMENT" => "figure",
            "CLASS" => "media $type",
            "DATA-TARGET" => "budkit-player",
            "ALLOWFULLSCREEN" => "true",
            "WIDTH" => !empty($width) ? $width : 100,
            "HEIGHT" => !empty($height) ? $height : 100,
            "CHILDREN" => array( $mediaTag, array("ELEMENT"=>"param", "NAME"=>"pluginpath" , "VALUE"=>$templatePath), )
        );
    }

    final private static function objectTag($src, $width, $height, $type){
        return array(
            "ELEMENT" => "object", "WIDTH"=>$width, "HEIGHT"=>$height,"TYPE"=>"application/x-shockwave-flash",
            "CHILDREN"=> array(
                array("ELEMENT"=>"param", "NAME"=>"movie", "VALUE"=>"flashmediaelement.swf"),
                array("ELEMENT"=>"param", "NAME"=>"flashvars", "VALUE"=>"controls=true&file={$src}" )
            )
        );
    }

    /**
     * Generates a video tag
     *
     * @param type $src
     * @param type $type
     * @param type $height
     * @param type $width
     * @param type $dataTag
     * @return type
     */
    final private static function videoTag($src, $type, $height, $width, $dataTag = array(), $extension) {

        $videoPlayerId = Framework::getRandomString(6);
        $videoFile =  \Library\Uri::externalize($src);

        return static::figureWrapper( array(
            "ELEMENT" => "video",
            "ID"=>$videoPlayerId,
            "WIDTH" => !empty($width) ? $width : "auto",
            "HEIGHT" => !empty($height) ? $height : "auto",
            "DATA-TARGET"=>"media-player",
            "CHILDREN" => array(
                array(
                    "ELEMENT" => "SOURCE",
                    "SRC" => $videoFile,
                    "TYPE" => $type
                ), static::objectTag($src, $width, $height, $type)
            )
        ), $width, $height, "video");
//        $videoPlayerId = Framework::getRandomString(6);
//        $videoFile =  \Library\Uri::externalize($src);
//        $videoPlayer = array(
//
//            array("ELEMENT"=>"div", "ID"=>"{$videoPlayerId}", "CLASS"=>"jp-video", "CHILDREN"=>array(
//                        array("ELEMENT"=>"div","CLASS"=>"jp-type-single","CHILDREN"=>array(
//                            array("ELEMENT"=>"div", "ID"=>"jquery_jplayer_{$videoPlayerId}", "CLASS"=>"jp-jplayer", "DATA-SOURCE"=>"{$videoFile}", "DATA-FORMATS"=>"{$extension}", "DATA-ANCESTOR"=>"#{$videoPlayerId}", "DATA-TARGET"=>"media-player"),
//                            array("ELEMENT"=>"div","CLASS"=>"jp-gui","CHILDREN"=>array(
//                                    array("ELEMENT"=>"div", "CLASS"=>"jp-video-play", "CHILDREN"=>array(array("ELEMENT"=>"a", "CLASS"=>"jp-video-play-icon", "TAB-INDEX"=>"1", "CDATA"=>"Play"))),
//                                    array("ELEMENT"=>"div","CLASS"=>"jp-interface", "CHILDREN"=>array(
//                                            static::getProgressTag(),
//                                            array("ELEMENT"=>"div", "CLASS"=>"jp-current-time"),
//                                            array("ELEMENT"=>"div", "CLASS"=>"jp-duration"),
//                                            array("ELEMENT"=>"div", "CLASS"=>"jp-controls-holder","CHILDREN"=>static::getControlsTag(),static::getVolumeTag(),static::getTogglesTag()),
//                                            static::getTitleTag()
//                                        )
//                                    ),
//                                )
//                            ), static::getUpgradeWarningTag()
//                        )
//                    )
//                )
//            )
//        );
        //return $figure;
        //return $videoPlayer;
    }

    final private static function getProgressTag(){
        return array("ELEMENT"=>"div", "CLASS"=>"jp-progress", "CHILDREN"=>array(
            array("ELEMENT"=>"div", "CLASS"=>"jp-seek-bar", "CHILDREN"=>array(
                array("ELEMENT"=>"div", "CLASS"=>"jp-play-bar")
            )
            )
        )
        );
    }

    final private static function getVolumeTag(){
        return array("ELEMENT"=>"div", "CLASS"=>"jp-volume-bar", "CHILDREN"=>array(
            array("ELEMENT"=>"div", "CLASS"=>"jp-volume-bar-value")
        )
        );
    }
    final private static function getTimeTag(){
        return array("ELEMENT"=>"div", "CLASS"=>"jp-time-holder", "CHILDREN"=>array(
            array("ELEMENT"=>"div", "CLASS"=>"jp-current-time"),
            array("ELEMENT"=>"div", "CLASS"=>"jp-duration")
        )
        );
    }
    final private static function getTitleTag(){
        return array("ELEMENT"=>"div", "CLASS"=>"jp-title", "CHILDREN"=>array(
            array("ELEMENT"=>"ul", "CHILDREN"=>array(
                array("ELEMENT"=>"li","CDATA"=>"Media Title"),
            )
            ),
        )
        );
    }

    final private static function getTogglesTag(){
        return array("ELEMENT"=>"ul", "CLASS"=>"jp-toggles", "CHILDREN"=>array(
            array("ELEMENT"=>"li", "CHILDREN"=>array( array("ELEMENT"=>"a","CLASS"=>"jp-full-screen","HREF"=>"javascript:;", "TAB-INDEX"=>"1","CHILDREN"=>array(array("ELEMENT"=>"i","CLASS"=>"fa fa-expand"))))),
            array("ELEMENT"=>"li", "CHILDREN"=>array( array("ELEMENT"=>"a","CLASS"=>"jp-restore-screen","HREF"=>"javascript:;", "TAB-INDEX"=>"1","CHILDREN"=>array(array("ELEMENT"=>"i","CLASS"=>"fa fa-compress")))))
        )
        );
    }
    final private static function getControlsTag(){
        return array("ELEMENT"=>"ul", "CLASS"=>"jp-controls", "CHILDREN"=>array(
            array("ELEMENT"=>"li", "CHILDREN"=>array( array("ELEMENT"=>"a","CLASS"=>"jp-play","HREF"=>"javascript:;", "TAB-INDEX"=>"1","CHILDREN"=>array(array("ELEMENT"=>"i","CLASS"=>"fa fa-play"))))),
            array("ELEMENT"=>"li", "CHILDREN"=>array( array("ELEMENT"=>"a","CLASS"=>"jp-pause","HREF"=>"javascript:;", "TAB-INDEX"=>"1","CHILDREN"=>array(array("ELEMENT"=>"i","CLASS"=>"fa fa-pause"))))),
            array("ELEMENT"=>"li", "CHILDREN"=>array( array("ELEMENT"=>"a","CLASS"=>"jp-stop","HREF"=>"javascript:;", "TAB-INDEX"=>"1","CHILDREN"=>array(array("ELEMENT"=>"i","CLASS"=>"fa fa-stop"))))),
            array("ELEMENT"=>"li", "CHILDREN"=>array( array("ELEMENT"=>"a","CLASS"=>"jp-mute","HREF"=>"javascript:;", "TAB-INDEX"=>"1","CHILDREN"=>array(array("ELEMENT"=>"i","CLASS"=>"fa fa-volume-off"))))),
            array("ELEMENT"=>"li", "CHILDREN"=>array( array("ELEMENT"=>"a","CLASS"=>"jp-unmute","HREF"=>"javascript:;", "TAB-INDEX"=>"1","CHILDREN"=>array(array("ELEMENT"=>"i","CLASS"=>"fa fa-volume-up"))))),
            //array("ELEMENT"=>"li", "CHILDREN"=>array( array("ELEMENT"=>"a","CLASS"=>"jp-volume-max","HREF"=>"javascript:;", "TAB-INDEX"=>"1","CHILDREN"=>array(array("ELEMENT"=>"i","CLASS"=>"fa fa-play"))))),
        )
        );
    }

    final private static function getUpgradeWarningTag(){
        return array("ELEMENT"=>"div", "CLASS"=>"jp-no-solution", "CHILDREN"=>array(
            array("ELEMENT"=>"div", "CLASS"=>"alert alert-warning", "CHILDREN"=>array(
                array("ELEMENT"=>"strong","CDATA"=>"Upgrade Required"),
                array("ELEMENT"=>"p","CDATA"=>"To play this media please upgrade your browser"),
            )
            ),
        )
        );
    }

    /**
     * Displays and audio media tag
     * @param type $src
     * @param type $type
     * @param type $height
     * @param type $width
     * @param type $dataTag
     * @return string
     */
    final private static function audioTag($src, $type, $height, $width, $dataTag = array(), $extension) {

//        $width = !empty($width) ? $width : \Library\Config::getParam('gallery-thumbnail-width', 170, 'content');
//        $height = !empty($height) ? $height : \Library\Config::getParam('gallery-thumbnail-height', 170, 'content');
//        $controls = static::mediaControls();
//        $audio = array(
//            "ELEMENT" => "audio",
//            "WIDTH" => $width,
//            "HEIGHT" => $height,
//            "CHILDREN" => array(
//                array(
//                    "ELEMENT" => "SOURCE",
//                    "SRC" => $src,
//                    "TYPE" => $type
//                )
//            )
//        );
//        $poster = array(
//            "ELEMENT" => "img",
//            "SRC" => \Library\Uri::internal("/system/object/placeholder/resize/170/170"),
//        );
//        $floating = array(
//            "ELEMENT" => "span", "CLASS" => "floating-controls",
//            "CHILDREN" => array(
//                array("ELEMENT" => "i", "CLASS" => "icon-play-circle"),
//            )
//        );
//        $figure = array(
//            "ELEMENT" => "figure",
//            "CLASS" => "audio",
//            "DATA-TARGET" => "budkit-player",
//            "ALLOWFULLSCREEN" => "true",
//            "CHILDREN" => array($poster, $floating, $audio, $controls)
//        );

        $audioPlayerId = Framework::getRandomString(6);
        $audioFile =  \Library\Uri::externalize($src);

        return static::figureWrapper( array(
            "ELEMENT" => "audio",
            "ID"=>$audioPlayerId,
            "WIDTH" => !empty($width) ? $width : "auto",
            "HEIGHT" => !empty($height) ? $height : "auto",
            "DATA-TARGET"=>"media-player",
            "CHILDREN" => array(
                array(
                    "ELEMENT" => "SOURCE",
                    "SRC" => $audioFile,
                    "TYPE" => $type
                ), static::objectTag($src, $width, $height, $type)
            )
        ), $width, $height, "audio");

//        $audioPlayer = array(
//            array("ELEMENT"=>"div", "ID"=>"jquery_jplayer_{$audioPlayerId}", "CLASS"=>"jp-jplayer", "DATA-SOURCE"=>"{$audioFile}", "DATA-ANCESTOR"=>"#{$audioPlayerId}", "DATA-FORMATS"=>"{$extension}",  "DATA-TARGET"=>"media-player"),
//            array("ELEMENT"=>"div", "ID"=>"{$audioPlayerId}", "CLASS"=>"jp-audio","CHILDREN"=>array(
//                array("ELEMENT"=>"div","CLASS"=>"jp-type-single","CHILDREN"=>array(
//                    array("ELEMENT"=>"div","CLASS"=>"jp-gui jp-interface","CHILDREN"=>array(
//                        static::getControlsTag(), static::getProgressTag(), static::getVolumeTag(), static::getTimeTag()
//                    )
//                    )
//                )
//                ),
//                static::getTitleTag(), static::getUpgradeWarningTag()
//            )
//            )
//        );
//        //return $figure;
//        return $audioPlayer;
    }


    /**
     * Execute the layout
     *
     * @param type $parser
     * @param type $tag
     * @return type
     */
    public static function execute($parser, $tag, $writer) {

        $attachmentModel = \Application\System\Models\Attachment::getInstance();
        $collectionModel = \Application\System\Models\Collection::getInstance();

        //@TODO media thumbnail mode
        if (!isset($tag['URI']) && empty($tag['URI']))
            return null;
        $uri = static::getData($tag['URI'], $tag['URI']);

        $mediaObject = $attachmentModel->loadObjectByURI($uri);
        $type = $mediaObject->getObjectType();

        if ($type !== "attachment"):
            //1.Load the collection!
            $collection = $collectionModel->loadObjectByURI($uri);
            //Now lets populate our collection with Items
            $collectionItems = $collection->getPropertyValue("collection_items");
            $collectionItemize = explode(",", $collectionItems);
            if (!empty($collectionItems) && is_array($collectionItemize) && !empty($collectionItemize)):

                $ul = array("ELEMENT" => "ul", "CLASS" => "media-grid compensate-margins top-media clearfix");
                $tag['WIDTH'] = \Library\Config::getParam('gallery-thumbnail-width', 200, 'content');
                $tag['HEIGHT'] = \Library\Config::getParam('gallery-thumbnail-height', 200, 'content');
                $tag['MODE'] = 'detailed';
                foreach ($collectionItemize as $item) {
                    $li = array("ELEMENT" => "li", "CHILDREN" => static::tag($attachmentModel->loadObjectByURI($item), $tag));
                    $ul["CHILDREN"][] = $li;
                }
                //Lots of child elements;
                $tag = array("ELEMENT" => "div", "CLASS" => "media clearfix", "CHILDREN"=>$ul);
            else:
                $tag = static::tag($mediaObject, $tag);
            endif;
        else:
            $tag = static::tag($mediaObject, $tag);
        endif;


        return $tag;
    }

    /**
     * Returns and instantiated Instance of the layout class
     *
     * NOTE: As of PHP5.3 it is vital that you include constructors in your class
     * especially if they are defined under a namespace. A method with the same
     * name as the class is no longer considered to be its constructor
     *
     * @staticvar object $instance
     * @property-read object $instance To determine if class was previously instantiated
     * @property-write object $instance
     * @return object layout
     */
    public static function getInstance() {
        if (is_object(static::$instance) && is_a(static::$instance, 'media'))
            return static::$instance;
        static::$instance = new self();
        return static::$instance;
    }

}

