<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * dropbox.php
 *
 * Requires PHP version 5.3
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 * @category   Utility
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/authenticate/oauth
 * @since      Class available since Release 1.0.0 Jan 14, 2012 4:54:37 PM
 * 
 */

namespace Platform\Authenticate\OAuth\Provider;

use Platform\Authenticate\OAuth;

/**
 * What is the purpose of this class, in one sentence?
 *
 * How does this class achieve the desired purpose?
 *
 * @category   Utility
 * @author     Phil Sturgeon (Original Author)
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @license    http://philsturgeon.co.uk/code/dbad-license
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/authenticate/oauth
 * @since      Class available since Release 1.0.0 Jan 14, 2012 4:54:37 PM
 */
class Dropbox extends OAuth\Provider {

    public $name = 'dropbox';

    /**
     * @var  string  the method to use when requesting tokens
     */
    public $method = 'POST';


    public function urlRequestToken() {
        //Not Needed for OAuth2.0;
    }

    public function urlAuthorize() {
        return 'https://www.dropbox.com/1/oauth2/authorize';
    }

    public function urlAccessToken() {
        return 'https://www.dropbox.com/1/oauth2/token';
    }

    public function getUserInfo() {

        $token =& func_get_arg(0); //Consumer

        if (!is_a($token, '\Platform\Authenticate\OAuth\Token\Access'))
            throw new \Platform\Exception('First Argument Passed to getUserInfo must be of type OAuth\Token\Access');

        //Get user information dropbox
        $url = 'https://api.dropbox.com/1/account/info?' . http_build_query(array(
          'access_token' => $token->accessToken,
        ));

        $user = json_decode( file_get_contents($url) );
        // Create a response from the request
        return array(
            'uid' => $user->uid,
            'name' => $user->display_name,
            'email' => null, //dropbo api does not expose the users email
            'location' => $user->country,
        );
    }

}