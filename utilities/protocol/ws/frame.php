<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * ssl.php
 *
 * Requires PHP version 5.3
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 * @category   Utility
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/protocol/http
 * @since      Class available since Release 1.0.0 Jan 14, 2012 4:54:37 PM
 *
 */

namespace Platform\Protocol\Ws;


/**
 * What is the purpose of this class, in one sentence?
 *
 * How does this class achieve the desired purpose?
 *
 * @category   Utility
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/output/protocol/http
 * @since      Class available since Release 1.0.0 Jan 14, 2012 4:54:37 PM
 */

use Platform\Protocol\Ws\Exception;
use Platform\Protocol\Ws\Payload;


/**
 * Represents a WebSocket frame
 */
abstract class Frame
{
    /**
     * The frame data length
     *
     * @var int
     */
    protected $length = null;

    /**
     * The type of this payload
     *
     * @var int
     */
    protected $type = null;

    /**
     * The buffer
     *
     * May not be a complete payload, because this frame may still be receiving
     * data. See
     *
     * @var string
     */
    protected $buffer = '';

    /**
     * The enclosed frame payload
     *
     * May not be a complete payload, because this frame might indicate a continuation
     * frame. See isFinal() versus isComplete()
     *
     * @var string
     */
    protected $payload = '';

    /**
     * Gets the length of the payload
     *
     * @throws FrameException
     * @return int
     */
    abstract public function getLength();

    /**
     * Resets the frame and encodes the given data into it
     *
     * @param string $data
     * @param int $type
     * @param boolean $masked
     * @return Frame
     */
    abstract public function encode($data, $type = Protocol::TYPE_TEXT, $masked = false);

    /**
     * Whether the frame is the final one in a continuation
     *
     * @return boolean
     */
    abstract public function isFinal();

    /**
     * @return int
     */
    abstract public function getType();

    /**
     * Decodes a frame payload from the buffer
     *
     * @return void
     */
    abstract protected function decodeFramePayloadFromBuffer();

    /**
     * Gets the expected length of the buffer once all the data has been
     *  receieved
     *
     * @return int
     */
    abstract protected function getExpectedBufferLength();

    /**
     * Whether the frame is complete
     *
     * @return boolean
     */
    public function isComplete()
    {
        if (!$this->buffer) {
            return false;
        }

        try {
            return $this->getBufferLength() >= $this->getExpectedBufferLength();
        } catch (Exception\Frame $e) {
            return false;
        }
    }

    /**
     * Receieves data into the frame
     *
     * @param string $buffer
     */
    public function receiveData($data)
    {
        $this->buffer .= $data;
    }

    /**
     * Gets the remaining number of bytes before this frame will be complete
     *
     * @return number
     */
    public function getRemainingData()
    {
        try {
            return $this->getExpectedBufferLength() - $this->getBufferLength();
        } catch (Exception\Frame $e) {
            return null;
        }
    }

    /**
     * Whether this frame is waiting for more data
     *
     * @return boolean
     */
    public function isWaitingForData()
    {
        return $this->getRemainingData() > 0;
    }

    /**
     * Gets the contents of the frame payload
     *
     * The frame must be complete to call this method.
     *
     * @return string
     */
    public function getFramePayload()
    {
        if (!$this->isComplete()) {
            throw new Exception\Frame('Cannot get payload: frame is not complete');
        }

        if (!$this->payload && $this->buffer) {
            $this->decodeFramePayloadFromBuffer();
        }

        return $this->payload;
    }

    /**
     * Gets the contents of the frame buffer
     *
     * This is the encoded value, receieved into the frame with receiveData().
     *
     * @throws FrameException
     * @return string binary
     */
    public function getFrameBuffer()
    {
        if (!$this->buffer && $this->payload) {
            throw new Exception\Frame('Cannot get frame buffer');
        }
        return $this->buffer;
    }

    /**
     * Gets the expected length of the frame payload
     *
     * @return int
     */
    protected function getBufferLength()
    {
        return strlen($this->buffer);
    }
}
