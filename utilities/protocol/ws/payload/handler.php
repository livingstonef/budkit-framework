<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * ssl.php
 *
 * Requires PHP version 5.3
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 * @category   Utility
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/protocol/http
 * @since      Class available since Release 1.0.0 Jan 14, 2012 4:54:37 PM
 *
 */
namespace Platform\Protocol\Ws\Payload;

use Platform\Protocol\Ws;
use Platform\Protocol\Ws\Utilities;
use Platform\Protocol\Ws\Exception;

use \InvalidArgumentException;

/**
 * What is the purpose of this class, in one sentence?
 *
 * How does this class achieve the desired purpose?
 *
 * @category   Utility
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/output/protocol/http
 * @since      Class available since Release 1.0.0 Jan 14, 2012 4:54:37 PM
 */

/**
 * Handles chunking and splitting of payloads into frames
 */
class Handler extends Utilities\Configurable
{
    /**
     * A callback that will be called when a complete payload is available
     *
     * @var callable
     */
    protected $callback;

    /**
     * The current payload
     */
    protected $payload;

    /**
     * @param callable $callback
     * @param array $options
     * @throws InvalidArgumentException
     */
    public function __construct($callback, array $options = array())
    {
        parent::__construct($options);

        if (!is_callable($callback)) {
            throw new InvalidArgumentException('You must supply a callback to PayloadHandler');
        }

        $this->callback = $callback;
    }

    /**
     * Handles the raw socket data given
     *
     * @param string $data
     */
    public function handle($data)
    {
        if (!$this->payload) {
            $this->payload = $this->protocol->getPayload();
        }

        while ($data) { // Each iteration pulls off a single payload chunk
            $size = strlen($data);
            $remaining = $this->payload->getRemainingData();

            // If we don't yet know how much data is remaining, read data into
            // the payload in two byte chunks (the size of a WebSocket frame
            // header to get the initial length)
            //
            // Then re-loop. For extended lengths, this will happen once or four
            // times extra, as the extended length is read in.
            if ($remaining === null) {
                $chunk_size = 2;
            } elseif ($remaining > 0) {
                $chunk_size = $remaining;
            } elseif ($remaining === 0) {
                $chunk_size = 0;
            }

            $chunk_size = min(strlen($data), $chunk_size);
            $chunk = substr($data, 0, $chunk_size);
            $data = substr($data, $chunk_size);

            $this->payload->receiveData($chunk);

            if ($remaining !== 0 && !$this->payload->isComplete()) {
                continue;
            }

            if ($this->payload->isComplete()) {
                $this->emit($this->payload);
                $this->payload = $this->protocol->getPayload();
            } else {
                throw new Exception\Payload('Payload will not complete');
            }
        }
    }

    /**
     * Get the current payload
     *
     * @return Payload
     */
    public function getCurrent()
    {
        return $this->getPayloadHandler->getCurrent();
    }

    /**
     * Emits a complete payload to the callback
     *
     * @param Payload $payload
     */
    protected function emit(Ws\Payload $payload)
    {
        call_user_func($this->callback, $payload);
    }
}