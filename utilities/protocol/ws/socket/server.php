<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * ssl.php
 *
 * Requires PHP version 5.3
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 * @category   Utility
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/protocol/http
 * @since      Class available since Release 1.0.0 Jan 14, 2012 4:54:37 PM
 *
 */
namespace Platform\Protocol\Ws\Socket;

use Platform\Protocol\Ws;
use Platform\Protocol\Ws\Exception;

/**
 * What is the purpose of this class, in one sentence?
 *
 * How does this class achieve the desired purpose?
 *
 * @category   Utility
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/output/protocol/http
 * @since      Class available since Release 1.0.0 Jan 14, 2012 4:54:37 PM
 */

/**
 * Server socket
 *
 * Used for a server's "master" socket that binds to the configured
 * interface and listens
 */
class Server extends Uri
{
    const TIMEOUT_ACCEPT = 5;

    /**
     * Whether the socket is listening
     *
     * @var boolean
     */
    protected $listening = false;

    /**
     * @see Wrench\Socket.Socket::configure()
     *   Options include:
     *     - backlog               => int, used to limit the number of outstanding
     *                                 connections in the socket's listen queue
     *     - ssl_cert_file         => string, server SSL certificate
     *                                 file location. File should contain
     *                                 certificate and private key
     *     - ssl_passphrase        => string, passphrase for the key
     *     - timeout_accept        => int, seconds, default 5
     */
    protected function configure(array $options)
    {
        $options = array_merge(array(
            'backlog'               => 50,
            'ssl_cert_file'         => null,
            'ssl_passphrase'        => null,
            'ssl_allow_self_signed' => false,
            'timeout_accept'        => self::TIMEOUT_ACCEPT
        ), $options);

        parent::configure($options);
    }

    /**
     * Listens
     *
     * @throws ConnectionException
     */
    public function listen()
    {
        $this->socket = stream_socket_server(
            $this->getUri(),
            $errno,
            $errstr,
            STREAM_SERVER_BIND|STREAM_SERVER_LISTEN,
            $this->getStreamContext()
        );

        if (!$this->socket) {
            throw new Exception\Connection(sprintf(
                'Could not listen on socket: %s (%d)',
                $errstr,
                $errno
            ));
        }

        $this->listening = true;
    }

    /**
     * Accepts a new connection on the socket
     *
     * @throws ConnectionException
     * @return resource
     */
    public function accept()
    {
        $new = stream_socket_accept(
            $this->socket,
            $this->options['timeout_accept']
        );

        if (!$new) {
            throw new Exception\Connection(socket_strerror(socket_last_error($new)));
        }

        return $new;
    }

    /**
     * @see Wrench\Socket.UriSocket::getSocketStreamContextOptions()
     */
    protected function getSocketStreamContextOptions()
    {
        $options = array();

        if ($this->options['backlog']) {
            $options['backlog'] = $this->options['backlog'];
        }

        return $options;
    }

    /**
     * @see Wrench\Socket.UriSocket::getSslStreamContextOptions()
     */
    protected function getSslStreamContextOptions()
    {
        $options = array();

        if ($this->options['server_ssl_cert_file']) {
            $options['local_cert'] = $this->options['server_ssl_cert_file'];
            if ($this->options['server_ssl_passphrase']) {
                $options['passphrase'] = $this->options['server_ssl_passphrase'];
            }
        }

        return $options;
    }
}