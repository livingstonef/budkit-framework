<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * ssl.php
 *
 * Requires PHP version 5.3
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 * @category   Utility
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/protocol/http
 * @since      Class available since Release 1.0.0 Jan 14, 2012 4:54:37 PM
 *
 */
namespace Platform\Protocol\Ws\Socket;

use Platform\Protocol\Ws;


/**
 * What is the purpose of this class, in one sentence?
 *
 * How does this class achieve the desired purpose?
 *
 * @category   Utility
 * @author     Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * @copyright  1997-2012 Stonyhills HQ
 * @license    http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version    Release: 1.0.0
 * @link       http://stonyhillshq/documents/index/carbon4/libraries/output/protocol/http
 * @since      Class available since Release 1.0.0 Jan 14, 2012 4:54:37 PM
 */

abstract class Uri extends Ws\Socket
{
    protected $scheme;
    protected $host;
    protected $port;

    /**
     * URI Socket constructor
     *
     * @param string $uri     WebSocket URI, e.g. ws://example.org:8000/chat
     * @param array  $options (optional)
     *   Options:
     *     - protocol             => Wrench\Protocol object, latest protocol
     *                                 version used if not specified
     *     - timeout_socket       => int, seconds, default 5
     *     - server_ssl_cert_file => string, server SSL certificate
     *                                 file location. File should contain
     *                                 certificate and private key
     *     - server_ssl_passphrase => string, passphrase for the key
     *     - server_ssl_allow_self_signed => boolean, whether to allows self-
     *                                 signed certs
     */
    public function __construct($uri, array $options = array())
    {
        parent::__construct($options);

        list($this->scheme, $this->host, $this->port)
            = $this->protocol->validateSocketUri($uri);
    }

    /**
     * Gets the canonical/normalized URI for this socket
     *
     * @return string
     */
    protected function getUri()
    {
        return sprintf(
            '%s://%s:%d',
            $this->scheme,
            $this->host,
            $this->port
        );
    }

    /**
     * @todo DNS lookup? Override getIp()?
     * @see Wrench\Socket.Socket::getName()
     */
    protected function getName()
    {
        return sprintf('%s:%s', $this->host, $this->port);
    }

    /**
     * Gets the host name
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @see Wrench\Socket.Socket::getPort()
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Gets a stream context
     */
    protected function getStreamContext($listen = false)
    {
        $options = array();

        if ($this->scheme == Ws\Protocol::SCHEME_UNDERLYING_SECURE
            || $this->scheme == Ws\Protocol::SCHEME_UNDERLYING) {
            $options['socket'] = $this->getSocketStreamContextOptions();
        }

        if ($this->scheme == Ws\Protocol::SCHEME_UNDERLYING_SECURE) {
            $options['ssl'] = $this->getSslStreamContextOptions();
        }

        return stream_context_create(
            $options,
            array()
        );
    }

    /**
     * Returns an array of socket stream context options
     *
     * See http://php.net/manual/en/context.socket.php
     *
     * @return array
     */
    abstract protected function getSocketStreamContextOptions();

    /**
     * Returns an array of ssl stream context options
     *
     * See http://php.net/manual/en/context.ssl.php
     *
     * @return array
     */
    abstract protected function getSslStreamContextOptions();
}
